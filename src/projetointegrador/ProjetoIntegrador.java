/*
    Projeto: Jogo RPG Textual
    Autor: João Max de Oliveira Santos
    Repositorio: https://bitbucket.org/joaomosantos
*/

package projetointegrador;

// Importações
import com.thoughtworks.xstream.XStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;

public class ProjetoIntegrador {
    
    // Instanciando(característica) e criando um objeto Scanner, para funcionar a entrada de dados por teclado
    static Scanner entrada = new Scanner(System.in);
    // Hash de Criptografia (É concatenado junto com a senha para deixa-lo mais segura)
    static String hash = "P!@y3r$";
    // Instanciando biblioteca que permite ler e gravar registro (memory card) em XML
    static XStream xstream = new XStream();
    // Variavel responsável por ler e/ou gravar o XML
    static String xml;
    
    public static void main(String[] args) {
        // Carregar XML
        xml = ler();
        // Com esse alias, cada dados do jogador armazenado no XML é separado pela tag 'Player' ao invés do default 'ProjetoIntegrador.Players'
        xstream.alias("Player", Players.class);
        
        // Instanciando(característica) e criando uma lista Players, que vai armazenar os dados dos jogadores
        ArrayList <Players> player = new ArrayList<>();
        
        // Irá armazenar a opção escolhida do menu
        int menu;
        
        do{
            System.out.println("========================");
            System.out.println("| RPG TEXTUAL: A FESTA |");
            System.out.println("========================");
            System.out.println("|     OPCOES:          |");
            System.out.println("|     1 - NEW GAME     |");
            System.out.println("|     2 - LOAD GAME    |");
            System.out.println("|     3 - HISTÓRIA     |");
            System.out.println("|     4 - CRÉDITOS     |");
            System.out.println("|     0 - SAIR         |");
            System.out.println("========================");
            System.out.print("OPCAO: ");
            menu = Integer.parseInt(entrada.nextLine());
            System.out.println("========================");
            
            switch(menu){
                case 0:
                    System.out.println(ConsoleColors.RED + "[O JOGO FOI FECHADO]" + ConsoleColors.RESET);
                    System.exit(0);
                    break;
                    
                case 1:
                    NewGame(player);
                    break;
                    
                case 2:
                    LoadGame();
                    break;
                    
                case 3:
                    Historia();
                    break;
                    
                case 4:
                    Creditos();
                    break;
                
                default:
                    System.out.println(ConsoleColors.RED + "[A OPÇÃO SELECIONADA É INVALIDA]" + ConsoleColors.RESET);
                    break;
            }
            
        }while(menu != 0);
    }
    
    public static void NewGame(ArrayList player) {
        // Se o XML nao estiver vazio ele vai recuperar os dados anteriores
        if (xml.length() > 0) {
            // ArrayList que vai recuperar todos os dados dos jogadores antigos (Old Players) do XML
            ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
            Players recuperar = null;
            for (int i = 0; i < oldPlayer.size(); i++) {
                // Recuperando cada registro dos jogadores antigos (memory card)
                recuperar = (Players) oldPlayer.get(i);
                // Adicionando no novo ArrayList atual, para quando for gravar não substituir os dados dos antigos jogadores
                player.add(recuperar);
                // Gravando no XML
                gravar(player);
            }
        }
        
        System.out.println("Olá, Bem-vindo!");
        System.out.println("Para iniciarmos o jogo precisamos de algumas informações do jogador");
        
        // Instancia que vai pegar os dados do jogador e depois armazenar em um ArrayList
        Players cadastro = new Players();
        // Recuperando o Id do jogador, como base no tamanho (size) do ArrayList
        cadastro.setId(player.size());
        // Recuperando os dados do jogador
        System.out.print("Informe seu nome: ");
        cadastro.setNome(entrada.nextLine());
        System.out.print("Informe seu e-mail: ");
        cadastro.setEmail(entrada.nextLine());
        System.out.print("Informe uma senha: ");
        cadastro.setSenha(Criptografar(entrada.nextLine() + hash));
        
        // Armazenando os dados do jogador em um ArrayList
        player.add(cadastro);
        // Gravando os dados do ArrayList no XML
        gravar(player);
        // Limpando o ArrayList, para quando for armazenar novamente, não ficar com os dados duplicados
        player.clear();
        
        System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
        // Iniciando o progresso
        progresso1(cadastro.getId(), cadastro.getNome(), cadastro.getVida());
    }
    
    public static void progresso1(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
               
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " tem participado de varias festas com seus amigos," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: e ele(a) tem bebido muito, e constante, e isso tem prejudicado muito a sua saúde e principalmente a sua memória," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: já se faz 3 dias que " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " está dormindo desde a ultima festa." + ConsoleColors.RESET);
        System.out.println("[" + nome + "]: zzzZZZZZzzZZzzZZZZZzzz");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: De repente " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " acorda, e pega seu celular para consultar as horas." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA PEGAR O CELULAR]");
        entrada.nextLine();
        System.out.println("[CELULAR]: " + Calendario());
        System.out.println("[" + nome + "]: Nossa!, não acredito que dormir por 3 dias, preciso ir ao supermercado correndo para comprar umas coisas para a festa de hoje.");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " se arruma com muita pressa, escova os dentes e pega a chave do carro para ir ao supermercado," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: ao entrar no carro, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " fala:" + ConsoleColors.RESET);
        System.out.println("[" + nome + "]: Puts, não lembro qual o caminho mais rápido para chegar ao supermercado, não posso me atrasar!");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: mas por incrível que pareça o(a) " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " lembrou de um conceito chamado 'GRAFOS'," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: porém, é apenas o conceito, ele precisará de sua ajuda para conseguir chegar no supermercado o mais rápido possível."+ ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Os conceito que ele lembrou são esses:" + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA LER OS CONCEITOS]");
        entrada.nextLine();
        System.out.println(ConsoleColors.PURPLE + "Um Grafo G = (V,A) é uma estrutura composta por um conjunto 'V' de elementos chamados vértices ou nós, e um conjunto 'A' de pares de vértices, chamados arcos ou arestas." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Um grafo é ponderado quando suas arestas possuem um peso. Ou valores associados." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Vértices -> É cada uma das entidades representadas em um grafo. Podem ser as pessoas, os pontos de uma rota, etc." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Arestas -> Faz a ligação entre dois vértices. Estabelecendo assim a relação entre eles." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Ex: (vértices –> cidades; arestas -> estradas)" + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Com base nesses conceitos, temos uma matriz bidimensional, onde uma das dimensões são vértices e a outra dimensão são arestas." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Quando a direção chega ao vértice o valor representado é negativo e quando sai é positivo." + ConsoleColors.RESET);
        System.out.println("Matriz de Incidência:");
        System.out.println("            |   A   |   B   |   C   |   D   |   E   ");
        System.out.println("    10km    |   0   |   0   |   1   |  -1   |   0   ");
        System.out.println("    35km    |   0   |   1   |   0   |   0   |  -1   ");
        System.out.println("    01km    |   0   |  -1   |   1   |   0   |   0   ");
        System.out.println("    02km    |   0   |   0   |   0   |   1   |  -1   ");
        System.out.println("    15km    |   1   |  -1   |   0   |   0   |   0   ");
        System.out.println("    20km    |   0   |   1   |   0   |  -1   |   0   ");
        System.out.println("    05km    |   0   |   0   |  -1   |   0   |   1   ");
        System.out.println("    20km    |   1   |   0   |  -1   |   0   |   0   ");
        System.out.println(ConsoleColors.BLUE + "\n\n[NARRADOR]: Considere que " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " esteja na cidade (A), e o supermercado se encontra na cidade (E)." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Qual o menor trajeto para chegar até o supermercado?" + ConsoleColors.RESET);
        
        // Adicionando lista de questões
        LinkedList questoes = new LinkedList();
        questoes.add("A -> C -> D -> E"); // Correta
        questoes.add("A -> B -> E");
        questoes.add("A -> C -> B -> E");
        questoes.add("A -> B -> D -> E");
        
        // Vai armazenar a questão correta
        String correta =  questoes.get(0).toString();
        // Embaralhando as Questões
        Collections.shuffle(questoes);
        
        // Vai me retornar o numero da questão correta, depois de ter embaralhado
        int questao = QuestaoCorreta(questoes, correta);
        
        // Armazena o valor da questão
        int alternativa;
        
        // do while, vai exibir mensagem de erro caso a opçao nao corresponder        
        do{
            // Exibindo as alternativa
            for(int i = 0; i < questoes.size(); i++) {
                System.out.println(i+1 + ") " + questoes.get(i));
            }

            System.out.println("DIGITE O NÚMERO DA QUESTÃO CORRETA: ");
            alternativa = Integer.parseInt(entrada.nextLine());
            if(alternativa < 1 || alternativa > 4)
                System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
        }while(alternativa < 1 || alternativa > 4);
        
        if(alternativa == questao) {
            System.out.println(ConsoleColors.GREEN + "[PARABÉNS, VOCÊ ACERTOU!]" + ConsoleColors.RESET);
            int opcao;
            do{
                System.out.println("[DESEJA SALVAR O JOGO?]");
                System.out.println("1) SIM");
                System.out.println("0) NÂO");
                System.out.println("DIGITE O NÚMERO: ");
                opcao = Integer.parseInt(entrada.nextLine());
                if(opcao < 0 || opcao > 1)
                    System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
            }while(opcao < 0 || opcao > 1);
            
            // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
            if(opcao == 1) {
                for (int i = 0; i < oldPlayer.size(); i++) {
                    recuperar = (Players) oldPlayer.get(i);
                    if(id == recuperar.getId()){
                        oldPlayer.remove(id);
                        recuperar.setProgresso(2);
                        recuperar.setVida(vida);
                        oldPlayer.add(id,recuperar);
                        gravar(oldPlayer);
                    }
                }
                System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
            }
            
            System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
            entrada.nextLine();
            // Inicia progresso 2
            progresso2(id, nome, vida);
        } else {
           vida--;
           System.out.println(ConsoleColors.RED + "[POR TER ERRADO O MENOR TRAJETO, O(A) " + ConsoleColors.RESET + nome + ConsoleColors.RED + " CHEGARÁ ATRASADO AO SEU DESTINO]" + ConsoleColors.RESET);
           if(vida <= 0) {
                System.out.println(ConsoleColors.RED + "[VOCÊ PERDEU TODAS AS VIDAS, POR CAUSA DISSO O JOGO SERÁ ENCERRADO!]" + ConsoleColors.RESET);
                System.exit(0);
           } else {
                System.out.println(ConsoleColors.RED + "[VOCÊ ACABOU DE PERDER 1 VIDA, RESTAM " + vida + "/2]"+ ConsoleColors.RESET);
                int opcao;
                do{
                    System.out.println("[DESEJA SALVAR O JOGO?]");
                    System.out.println("1) SIM");
                    System.out.println("0) NÂO");
                    System.out.println("DIGITE O NÚMERO: ");
                    opcao = Integer.parseInt(entrada.nextLine());
                    if(opcao < 0 || opcao > 1)
                        System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
                }while(opcao < 0 || opcao > 1);
                
                // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
                if(opcao == 1) {
                    for (int i = 0; i < oldPlayer.size(); i++) {
                        recuperar = (Players) oldPlayer.get(i);
                        if(id == recuperar.getId()){
                            oldPlayer.remove(id);
                            recuperar.setProgresso(2);
                            recuperar.setVida(vida);
                            oldPlayer.add(id,recuperar);
                            gravar(oldPlayer);
                        }
                    }
                    System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
                }

                System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                entrada.nextLine();
                // Inicia progresso 2
                progresso2(id, nome, vida);
           }
        }
    }
    
    public static void progresso2(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
        
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Finalmente, depois de um longo caminho " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " chega ao supermercado." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " sai do carro, pega um cesta e entra no supermercado, mas ele(a) não lembra exatamente o que precisa comprar, " + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: porém ele teve um grande ideia" + ConsoleColors.RESET);
        System.out.println("[" + nome + "]: Vou ligar para meu amigo e perguntar o que realmente preciso comprar para a festa de hoje");
        System.out.println("[PRESSIONE ENTER PARA PEGAR O CELULAR]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Com o celular na mão, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " liga para o amigo e começa a conversar..." + ConsoleColors.RESET);
        System.out.println("[" + nome + "]: blablabla blablabla blablabla");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Como " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " é muito esquecido(a), o seu amigo resolve enviar por e-mail a lista de compras" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Só " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " não tem franquia de internet para poder acessar os seus e-mail," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: mas, por incrível que pareça, o supermercado tem um local específico para acessar o e-mail e imprimir a lista de compras." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Chegando lá, a moça explica para " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " que no supermercado tem apenas uma impressora e ela é muito requisitada, e a todo instante tem alguem imprimindo." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: E ela continua..." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.CYAN + "[MOÇA]: Só é possivel enviar 5 documentos na fila de impressão, conforme for imprimindo, ai você pode enviar outros documentos." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Com essas informações, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " lembra de um conceito chamado 'FILA'" + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA LER OS CONCEITOS]");
        entrada.nextLine();
        System.out.println(ConsoleColors.PURPLE + "Fila, também chamado de FIFO (First In, First Out , primeiro a entrar, primeiro a sair) é o nome dado a estrutura de dados em que ocorrem inserção de dados em um extremo e sua saída por outro," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "obedecendo assim 'a ordem de chegada' como se fosse uma fila comum de pessoas." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "As filas tem duas operações básicas:" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Enqueue: enfileira um elemento." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Dequeue: desenfileira um elemento." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Com base nesse conceito, considere uma FILA Q com os elementos {4, 7, 9}, adicionados da esquerda para a direita, com críterio de entrada e saída FIFO"+ ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: (First In, First Out) e com a aplicação das sequintes operações:"+ ConsoleColors.RESET);
        System.out.println("Q.enqueue(4);");
        System.out.println("Q.enqueue(8);");
        System.out.println("Q.dequeue();");
        System.out.println("Q.enqueue(4);");
        System.out.println("Q.dequeue();");
        System.out.println("Q.dequeue();");
        System.out.println("Q.enqueue(8);");
        System.out.println("Q.dequeue();");
        System.out.println("Q.enqueue(5);");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Como ficou a fila de impressão após essas operações?" + ConsoleColors.RESET);
        
        // Adicionando lista de questões
        LinkedList questoes = new LinkedList();
        questoes.add("{4, 8, 4, 8}");
        questoes.add("{8, 4, 8, 5}"); // Correta
        questoes.add("{9, 7, 4, 4}");
        questoes.add("{5, 8, 4, 8}");
                
        // Vai armazenar a questão correta
        String correta =  questoes.get(1).toString();
        // Embaralhando as Questões
        Collections.shuffle(questoes);
        
        // Vai me retornar o numero da questão correta, depois de ter embaralhado
        int questao = QuestaoCorreta(questoes, correta);
        
        // Armazena o valor da questão
        int alternativa;
        
        // do while, vai exibir mensagem de erro caso a opçao nao corresponder        
        do{
            // Exibindo as alternativa
            for(int i = 0; i < questoes.size(); i++) {
                System.out.println(i+1 + ") " + questoes.get(i));
            }

            System.out.println("DIGITE O NÚMERO DA QUESTÃO CORRETA: ");
            alternativa = Integer.parseInt(entrada.nextLine());
            if(alternativa < 1 || alternativa > 4)
                System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
        }while(alternativa < 1 || alternativa > 4);
        
        if(alternativa == questao) {
            System.out.println(ConsoleColors.GREEN + "[PARABÉNS, VOCÊ ACERTOU!]" + ConsoleColors.RESET);
            int opcao;
            do{
                System.out.println("[DESEJA SALVAR O JOGO?]");
                System.out.println("1) SIM");
                System.out.println("0) NÂO");
                System.out.println("DIGITE O NÚMERO: ");
                opcao = Integer.parseInt(entrada.nextLine());
                if(opcao < 0 || opcao > 1)
                    System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
            }while(opcao < 0 || opcao > 1);
            
            // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
            if(opcao == 1) {
                for (int i = 0; i < oldPlayer.size(); i++) {
                    recuperar = (Players) oldPlayer.get(i);
                    if(id == recuperar.getId()){
                        oldPlayer.remove(id);
                        recuperar.setProgresso(3);
                        recuperar.setVida(vida);
                        oldPlayer.add(id,recuperar);
                        gravar(oldPlayer);
                    }
                }
                System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
            }
            
            System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
            entrada.nextLine();
            // Inicia progresso 3
            progresso3(id, nome, vida);
        } else {
           vida--;
           System.out.println(ConsoleColors.RED + "[POR TER ERRADO QUAIS DOCUMENTOS ESTÃO NA FILA DE IMPRESSÃO, O(A) " + ConsoleColors.RESET + nome + ConsoleColors.RED + " NÃO SABERÁ SE O SEU DOCUMENTO JÁ FOI IMPRESSO, POR CAUSA DISSO ATRASARÁ SUAS COMPRAS]" + ConsoleColors.RESET);
           if(vida <= 0) {
                System.out.println(ConsoleColors.RED + "[VOCÊ PERDEU TODAS AS VIDAS, POR CAUSA DISSO O JOGO SERÁ ENCERRADO!]" + ConsoleColors.RESET);
                System.exit(0);
           } else {
                System.out.println(ConsoleColors.RED + "[VOCÊ ACABOU DE PERDER 1 VIDA, RESTAM " + vida + "/2]"+ ConsoleColors.RESET);
                int opcao;
                do{
                    System.out.println("[DESEJA SALVAR O JOGO?]");
                    System.out.println("1) SIM");
                    System.out.println("0) NÂO");
                    System.out.println("DIGITE O NÚMERO: ");
                    opcao = Integer.parseInt(entrada.nextLine());
                    if(opcao < 0 || opcao > 1)
                        System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
                }while(opcao < 0 || opcao > 1);
                
                // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
                if(opcao == 1) {
                    for (int i = 0; i < oldPlayer.size(); i++) {
                        recuperar = (Players) oldPlayer.get(i);
                        if(id == recuperar.getId()){
                            oldPlayer.remove(id);
                            recuperar.setProgresso(3);
                            recuperar.setVida(vida);
                            oldPlayer.add(id,recuperar);
                            gravar(oldPlayer);
                        }
                    }
                    System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
                }

                System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                entrada.nextLine();
                // Inicia progresso 3
                progresso3(id, nome, vida);
           }
        }
    }
    
    public static void progresso3(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
        
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Após alguns minutos esperando a impressão de seu documento," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: finalmente a lista de compras foi impressa." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Agora, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " poderá dar inicio as suas compras." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA INICIAR AS COMPRAS]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " pega os produto que estão na lista e vai colocando no carrinho." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Chegando na sessão de bebida, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " precisa pegar algumas latinha de cerveja," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: porém os produto estão empilhado, e a marca de sua preferencia encontra-se em baixo." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Para pegar a latinha de sua preferencia, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " lembra de um conceito chamado 'PILHA'." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA LER OS CONCEITOS]");
        entrada.nextLine();
        System.out.println(ConsoleColors.PURPLE + "Pilha é estrutura de dados baseado no princípio de Last In First Out (LIFO)," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "ou seja 'o último que entra é o primeiro que sai' caracterizando um empilhamento de dados." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "As duas operações princípais de pilha são: push (empilhar) que adiciona um elemento no topo da pilha " + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "e pop (desempilhar) que remove o último elemento adicionado." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Considere três pilhas P, Q e R, onde P representa seu carrinho," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: inicialmente as pilhas estão assim P = {}, Q = {5, 88} e R = {9, 15, 30, 22, 40}" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: seus dados são inseridos da esquerda para a direita." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: E executando os seguintes metódos abaixo:" + ConsoleColors.RESET);
        System.out.println("P.push(Q.pop());");
        System.out.println("P.push(Q.pop());");
        System.out.println("Q.push(R.pop());");
        System.out.println("Q.push(R.pop());");
        System.out.println("Q.push(P.pop());");
        System.out.println("R.pop();");
        System.out.println("Q.push(R.pop());");
        System.out.println("R.push(P.pop());");
        System.out.println("P.push(Q.pop());");
        System.out.println("P.push(Q.pop());");
        System.out.println("P.push(Q.pop());");
        System.out.println("Q.push(30);");
        System.out.println(ConsoleColors.BLUE + "Como ficará seu carrinho?" + ConsoleColors.RESET);
        
        // Adicionando lista de questões
        LinkedList questoes = new LinkedList();
        questoes.add("P = {22, 5, 15}, Q = {30, 40} e R = {9, 88}");
        questoes.add("P = {15, 22}, Q = {40, 5, 30} e R = {9, 88, 30}");
        questoes.add("P = {15, 5, 22}, Q = {40, 30} e R = {9, 88}"); // Correta
        questoes.add("P = {22}, Q = {40, 30, 5} e R = {9, 15, 88}");
        
        // Vai armazenar a questão correta
        String correta =  questoes.get(2).toString();
        // Embaralhando as Questões
        Collections.shuffle(questoes);
        
        // Vai me retornar o numero da questão correta, depois de ter embaralhado
        int questao = QuestaoCorreta(questoes, correta);
        
        // Armazena o valor da questão
        int alternativa;
        
        // do while, vai exibir mensagem de erro caso a opçao nao corresponder        
        do{
            // Exibindo as alternativa
            for(int i = 0; i < questoes.size(); i++) {
                System.out.println(i+1 + ") " + questoes.get(i));
            }

            System.out.println("DIGITE O NÚMERO DA QUESTÃO CORRETA: ");
            alternativa = Integer.parseInt(entrada.nextLine());
            if(alternativa < 1 || alternativa > 4)
                System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
        }while(alternativa < 1 || alternativa > 4);
        
        if(alternativa == questao) {
            System.out.println(ConsoleColors.GREEN + "[PARABÉNS, VOCÊ ACERTOU!]" + ConsoleColors.RESET);
            int opcao;
            do{
                System.out.println("[DESEJA SALVAR O JOGO?]");
                System.out.println("1) SIM");
                System.out.println("0) NÂO");
                System.out.println("DIGITE O NÚMERO: ");
                opcao = Integer.parseInt(entrada.nextLine());
                if(opcao < 0 || opcao > 1)
                    System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
            }while(opcao < 0 || opcao > 1);
            
            // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
            if(opcao == 1) {
                for (int i = 0; i < oldPlayer.size(); i++) {
                    recuperar = (Players) oldPlayer.get(i);
                    if(id == recuperar.getId()){
                        oldPlayer.remove(id);
                        recuperar.setProgresso(4);
                        recuperar.setVida(vida);
                        oldPlayer.add(id,recuperar);
                        gravar(oldPlayer);
                    }
                }
                System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
            }
            
            System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
            entrada.nextLine();
            // Inicia progresso 4
            progresso4(id, nome, vida);
        } else {
           vida--;
           System.out.println(ConsoleColors.RED + "[POR TER CONFUNDIDO AS PILHAS QUE ESTÃO NO CARRINHO, O(A) " + ConsoleColors.RESET + nome + ConsoleColors.RED + " ATRASARÁ SUAS COMPRAS]" + ConsoleColors.RESET);
           if(vida <= 0) {
                System.out.println(ConsoleColors.RED + "[VOCÊ PERDEU TODAS AS VIDAS, POR CAUSA DISSO O JOGO SERÁ ENCERRADO!]" + ConsoleColors.RESET);
                System.exit(0);
           } else {
                System.out.println(ConsoleColors.RED + "[VOCÊ ACABOU DE PERDER 1 VIDA, RESTAM " + vida + "/2]"+ ConsoleColors.RESET);
                int opcao;
                do{
                    System.out.println("[DESEJA SALVAR O JOGO?]");
                    System.out.println("1) SIM");
                    System.out.println("0) NÂO");
                    System.out.println("DIGITE O NÚMERO: ");
                    opcao = Integer.parseInt(entrada.nextLine());
                    if(opcao < 0 || opcao > 1)
                        System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
                }while(opcao < 0 || opcao > 1);
                
                // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
                if(opcao == 1) {
                    for (int i = 0; i < oldPlayer.size(); i++) {
                        recuperar = (Players) oldPlayer.get(i);
                        if(id == recuperar.getId()){
                            oldPlayer.remove(id);
                            recuperar.setProgresso(4);
                            recuperar.setVida(vida);
                            oldPlayer.add(id,recuperar);
                            gravar(oldPlayer);
                        }
                    }
                    System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
                }

                System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                entrada.nextLine();
                // Inicia progresso 4
                progresso4(id, nome, vida);
           }
        }
    }
    
    public static void progresso4(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
        
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Agora com todos os seus produtos já no carrinho, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " vai ao caixa para efetuar pagamento de suas compras." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Chegando lá, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " se depara com os caixas superlotados e apenas o preferencial esta com a fila menor," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: então ele(a) resolve ir ao preferencial" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: E " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " lembra de um conceito de fila chamado 'DEQUE'" + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA LER OS CONCEITOS]");
        entrada.nextLine();
        System.out.println(ConsoleColors.PURPLE + "As deques (double ended queue) são filas duplamente ligadas, isto é, filas com algum tipo de prioridade, " + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "seus elementos podem ser adicionados ou removidos da frente (cabeça) ou de trás (cauda)." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "Seus principais métodos são addFirst (inserir no início), removeFirst (remover no início)," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.PURPLE + "addLast (inserir no fim) e removeLast (remover no fim)." + ConsoleColors.RESET);
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Considere uma fila F = {1, 2, 3, 4, 5}, onde o lado esquerdo representa o inicio/frente (cabeça)" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: e o lado direito representa fiim/trás (cauda)." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: E com a execução dos seguintes metódos:" + ConsoleColors.RESET);
        System.out.println("F.removeLast();");
        System.out.println("F.removeFirst();");
        System.out.println("F.addLast(6);");
        System.out.println("F.addLast(7);");
        System.out.println("F.removeFirst();");
        System.out.println("F.removeLast();");
        System.out.println("F.addFirst(8);");
        System.out.println("F.addFirst(9);");
        System.out.println("F.removeLast();");
        System.out.println("F.addLast(0)");
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: como ficará a fila? " + ConsoleColors.RESET);
        
        // Adicionando lista de questões
        LinkedList questoes = new LinkedList();
        questoes.add("F = {0, 3, 8, 9}");
        questoes.add("F = {9, 3, 8, 0}");
        questoes.add("F = {0, 8, 3, 9}"); 
        questoes.add("F = {9, 8, 3, 0}"); // Correta
        
        // Vai armazenar a questão correta
        String correta =  questoes.get(3).toString();
        // Embaralhando as Questões
        Collections.shuffle(questoes);
        
        // Vai me retornar o numero da questão correta, depois de ter embaralhado
        int questao = QuestaoCorreta(questoes, correta);
        
        // Armazena o valor da questão
        int alternativa;
        
        // do while, vai exibir mensagem de erro caso a opçao nao corresponder        
        do{
            // Exibindo as alternativa
            for(int i = 0; i < questoes.size(); i++) {
                System.out.println(i+1 + ") " + questoes.get(i));
            }

            System.out.println("DIGITE O NÚMERO DA QUESTÃO CORRETA: ");
            alternativa = Integer.parseInt(entrada.nextLine());
            if(alternativa < 1 || alternativa > 4)
                System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
        }while(alternativa < 1 || alternativa > 4);
        
        if(alternativa == questao) {
            System.out.println(ConsoleColors.GREEN + "[PARABÉNS, VOCÊ ACERTOU!]" + ConsoleColors.RESET);
            int opcao;
            do{
                System.out.println("[DESEJA SALVAR O JOGO?]");
                System.out.println("1) SIM");
                System.out.println("0) NÂO");
                System.out.println("DIGITE O NÚMERO: ");
                opcao = Integer.parseInt(entrada.nextLine());
                if(opcao < 0 || opcao > 1)
                    System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
            }while(opcao < 0 || opcao > 1);
            
            // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
            if(opcao == 1) {
                for (int i = 0; i < oldPlayer.size(); i++) {
                    recuperar = (Players) oldPlayer.get(i);
                    if(id == recuperar.getId()){
                        oldPlayer.remove(id);
                        recuperar.setProgresso(5);
                        recuperar.setVida(vida);
                        oldPlayer.add(id,recuperar);
                        gravar(oldPlayer);
                    }
                }
                System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
            }
            
            System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
            entrada.nextLine();
            // Inicia progresso 5
            progresso5(id, nome, vida);
        } else {
           vida--;
           System.out.println(ConsoleColors.RED + "[POR TER CONFUNDIDO AS PRIORIDADES DE UMA FILA, O(A) " + ConsoleColors.RESET + nome + ConsoleColors.RED + " ATRASARÁ PARA A FESTA]" + ConsoleColors.RESET);
           if(vida <= 0) {
                System.out.println(ConsoleColors.RED + "[VOCÊ PERDEU TODAS AS VIDAS, POR CAUSA DISSO O JOGO SERÁ ENCERRADO!]" + ConsoleColors.RESET);
                System.exit(0);
           } else {
                System.out.println(ConsoleColors.RED + "[VOCÊ ACABOU DE PERDER 1 VIDA, RESTAM " + vida + "/2]"+ ConsoleColors.RESET);
                int opcao;
                do{
                    System.out.println("[DESEJA SALVAR O JOGO?]");
                    System.out.println("1) SIM");
                    System.out.println("0) NÂO");
                    System.out.println("DIGITE O NÚMERO: ");
                    opcao = Integer.parseInt(entrada.nextLine());
                    if(opcao < 0 || opcao > 1)
                        System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
                }while(opcao < 0 || opcao > 1);
                
                // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
                if(opcao == 1) {
                    for (int i = 0; i < oldPlayer.size(); i++) {
                        recuperar = (Players) oldPlayer.get(i);
                        if(id == recuperar.getId()){
                            oldPlayer.remove(id);
                            recuperar.setProgresso(5);
                            recuperar.setVida(vida);
                            oldPlayer.add(id,recuperar);
                            gravar(oldPlayer);
                        }
                    }
                    System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
                }

                System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                entrada.nextLine();
                // Inicia progresso 5
                progresso5(id, nome, vida);
           }
        }
    }
    
    public static void progresso5(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
        
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Agora com as compras feita, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " precisa ir a festa na casa de seu amigo," + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " guarda as compras no porta-malas do carro e entrada dentro do carro." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Agora dentro do carro, " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " precisa encontrar um caminho mais rápido para chegar a casa de seu amigo" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: e com base no mesmo conceito de 'GRAFOS' que vimos anteriormente, considere a seguinte situação:" + ConsoleColors.RESET);
        System.out.println("Matriz de Adjacências:");
        System.out.println("         |   A   |   B   |   C   |   D   |   E   |   F   |   G   ");
        System.out.println("    A    |   0   |   0   |   0   |   1   |   0   |   0   |   0   ");
        System.out.println("    B    |   0   |   0   |   0   |   0   |   0   |   0   |   1   ");
        System.out.println("    C    |   0   |   0   |   1   |   1   |   0   |   0   |   0   ");
        System.out.println("    D    |   1   |   0   |   1   |   0   |   1   |   1   |   0   ");
        System.out.println("    E    |   0   |   0   |   0   |   1   |   0   |   1   |   0   ");
        System.out.println("    F    |   0   |   0   |   0   |   1   |   1   |   0   |   1   ");
        System.out.println("    G    |   0   |   1   |   0   |   0   |   0   |   1   |   0   ");
        System.out.println(ConsoleColors.BLUE + "\n\n[NARRADOR]: Considere que " + ConsoleColors.RESET + nome + ConsoleColors.BLUE + " esteja na cidade (A), e a casa de seu amigo se encontra na cidade (G)." + ConsoleColors.RESET);
        System.out.println(ConsoleColors.BLUE + "[NARRADOR]: Qual o menor roteiro para chegar a festa?" + ConsoleColors.RESET);
        
        // Adicionando lista de questões
        LinkedList questoes = new LinkedList();
        questoes.add("A -> D -> F -> G"); // Correta
        questoes.add("A -> B -> C -> G");
        questoes.add("A -> C -> E -> G");
        questoes.add("A -> B -> D -> G");
        
        // Vai armazenar a questão correta
        String correta =  questoes.get(0).toString();
        // Embaralhando as Questões
        Collections.shuffle(questoes);
        
        // Vai me retornar o numero da questão correta, depois de ter embaralhado
        int questao = QuestaoCorreta(questoes, correta);
        
        // Armazena o valor da questão
        int alternativa;
        
        // do while, vai exibir mensagem de erro caso a opçao nao corresponder        
        do{
            // Exibindo as alternativa
            for(int i = 0; i < questoes.size(); i++) {
                System.out.println(i+1 + ") " + questoes.get(i));
            }

            System.out.println("DIGITE O NÚMERO DA QUESTÃO CORRETA: ");
            alternativa = Integer.parseInt(entrada.nextLine());
            if(alternativa < 1 || alternativa > 4)
                System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
        }while(alternativa < 1 || alternativa > 4);
        
        if(alternativa == questao) {
            System.out.println(ConsoleColors.GREEN + "[PARABÉNS, VOCÊ ACERTOU!]" + ConsoleColors.RESET);
            int opcao;
            do{
                System.out.println("[DESEJA SALVAR O JOGO?]");
                System.out.println("1) SIM");
                System.out.println("0) NÂO");
                System.out.println("DIGITE O NÚMERO: ");
                opcao = Integer.parseInt(entrada.nextLine());
                if(opcao < 0 || opcao > 1)
                    System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
            }while(opcao < 0 || opcao > 1);
            
            // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
            if(opcao == 1) {
                for (int i = 0; i < oldPlayer.size(); i++) {
                    recuperar = (Players) oldPlayer.get(i);
                    if(id == recuperar.getId()){
                        oldPlayer.remove(id);
                        recuperar.setProgresso(6);
                        recuperar.setVida(vida);
                        oldPlayer.add(id,recuperar);
                        gravar(oldPlayer);
                    }
                }
                System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
            }
            
            System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
            entrada.nextLine();
            // final do jogo
            fim(id, nome, vida);
        } else {
           vida--;
           System.out.println(ConsoleColors.RED + "[POR TER ERRADO O MENOR ROTEIRO, O(A) " + ConsoleColors.RESET + nome + ConsoleColors.RED + " CHEGARÁ ATRASADO A FESTA]" + ConsoleColors.RESET);
           if(vida <= 0) {
                System.out.println(ConsoleColors.RED + "[VOCÊ PERDEU TODAS AS VIDAS, POR CAUSA DISSO O JOGO SERÁ ENCERRADO!]" + ConsoleColors.RESET);
                System.exit(0);
           } else {
                System.out.println(ConsoleColors.RED + "[VOCÊ ACABOU DE PERDER 1 VIDA, RESTAM " + vida + "/2]"+ ConsoleColors.RESET);
                int opcao;
                do{
                    System.out.println("[DESEJA SALVAR O JOGO?]");
                    System.out.println("1) SIM");
                    System.out.println("0) NÂO");
                    System.out.println("DIGITE O NÚMERO: ");
                    opcao = Integer.parseInt(entrada.nextLine());
                    if(opcao < 0 || opcao > 1)
                        System.out.println(ConsoleColors.RED + "[OPÇÃO INVALIDA! TENTE NOVAMENTE]" + ConsoleColors.RESET);
                }while(opcao < 0 || opcao > 1);
                
                // Se for igual a 1, irá recuperar os dados do jogador no XML, e irá atualizar
                if(opcao == 1) {
                    for (int i = 0; i < oldPlayer.size(); i++) {
                        recuperar = (Players) oldPlayer.get(i);
                        if(id == recuperar.getId()){
                            oldPlayer.remove(id);
                            recuperar.setProgresso(6);
                            recuperar.setVida(vida);
                            oldPlayer.add(id,recuperar);
                            gravar(oldPlayer);
                        }
                    }
                    System.out.println(ConsoleColors.GREEN + "[OS DADOS FORAM SALVO COM SUCESSO]" + ConsoleColors.RESET);
                }

                System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                entrada.nextLine();
                // final do jogo
                fim(id, nome, vida);
           }
        }
    }
    
    public static void fim(int id, String nome, int vida) {
        // Limpadando console
        LimparConsole();
        
        System.out.println("PARABÊNS! VOCÊ CONCLUIU O JOGO");
        System.out.println(nome + ", CONSEGUIU CHEGAR A FESTA A TEMPO! GRAÇAS AO SEU CONHECIMENTO.");
        System.out.println("THE END...");
    }
    
    public static void LoadGame() {
        // Recuperar XML
        ArrayList<Players> oldPlayer = (ArrayList<Players>) xstream.fromXML(xml);
        Players recuperar = null;
        
        int cont = 0;
        
        // Informando os dados para ser buscando no XML
        System.out.println("INFORME SEU E-MAIL");
        String email = entrada.nextLine();
        System.out.print("INFORME SUA SENHA: ");
        String senha = Criptografar(entrada.nextLine() + hash);
        
        for (int i = 0; i < oldPlayer.size(); i++) {
            recuperar = (Players) oldPlayer.get(i);
            // Se o e-mail e senha for iguais ao xml, ele retorna o progresso onde parou
            if(email.equals(recuperar.getEmail()) && senha.equals(recuperar.getSenha())) {
                switch(recuperar.getProgresso()) {
                    case 1:
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        progresso1(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    case 2:
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        progresso2(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    case 3:
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        progresso3(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    case 4: 
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        progresso4(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    case 5:
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        progresso5(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    case 6:
                        System.out.println(ConsoleColors.GREEN + "[O PROGRESSO FOI RESTAURADO COM SUCESSO]" + ConsoleColors.RESET);
                        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
                        entrada.nextLine();
                        fim(recuperar.getId(), recuperar.getNome(), recuperar.getVida());
                        break;
                    default:
                        System.out.println(ConsoleColors.RED + "[NÃO FOI POSSÍVEL RESTAURAR O PROGRESSO, POR FAVOR ENTRE EM CONTATO COM O ADMINISTRADOR]" + ConsoleColors.RESET);
                        break;
                }
                break;
            } else {
                cont++;
                if(cont == oldPlayer.size()) {
                    System.out.println(ConsoleColors.RED + "[NÃO EXISTE DADOS SALVO DO JOGADOR]" + ConsoleColors.RESET);
                }
            }
        }
        
    }
    
    public static void Historia() {
        System.out.println("=======================================================================================================================================");
        System.out.println("                                                        A HISTORIA                                                                     ");
        System.out.println("=======================================================================================================================================");
        System.out.println("Com uma lista de compras em mãos o personagem precisará efetuar algumas compras no supermercado,");
        System.out.println("porém em meio ao processo o mesmo encontrará diversas situações do nosso cotidiano a ser resolvidas, tais como:");
        System.out.println("fila de espera para efetuar pagamento, filas preferenciais, desempilhar um determinado produto e etc.");
        System.out.println("E para soluciona-lo ele precisará responder alguns conceitos de estrutura de dados e ao mesmo tempo despertar o aprendizado do jogador.");
        System.out.println("=======================================================================================================================================");
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
    }
    
    public static void Creditos() {
        System.out.println("===================================================");
        System.out.println("|    PROJETO INTEGRADOR 1 - JOGO TEXTUAL          |");
        System.out.println("|  DESENVOLVIDO POR: JOÃO MAX DE OLIVEIRA SANTOS  |");
        System.out.println("===================================================");
        System.out.println("[PRESSIONE ENTER PARA CONTINUAR]");
        entrada.nextLine();
    }
    
    public static String Calendario() {
        String [] mes = {"Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"};
        Calendar c = Calendar.getInstance();
        String data = c.get(Calendar.DAY_OF_MONTH) + " de " + mes[c.get(Calendar.MONTH)] + " de " + c.get(Calendar.YEAR);
        String horas = c.get(Calendar.HOUR_OF_DAY) + "h" + String.format("%02d", c.get(Calendar.MINUTE));
        return data + " - " + horas;
    }
    
    public static void LimparConsole() {
        for(int i = 0; i <= 50; i++) {
            // Quebra de linha
            System.out.println("\n");
        }
    }
    
    // Meotodo que vai me retornar a questão correta após ser embaralhada (Random)
    public static int QuestaoCorreta(LinkedList questoes, String QuestaoCorreta) {
        int correta = 0;
        for(int i = 0; i < questoes.size(); i++) {
            if(questoes.get(i).toString().equals(QuestaoCorreta)) {
                correta = questoes.indexOf(questoes.get(i));
            }
        }
        return ++correta;
    }
    
    // Criptografar senha
    public static String Criptografar(String str) {  
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(str.getBytes(), 0, str.length());
            String senha = new BigInteger(1, m.digest()).toString(16);
            return senha;
        } catch (Exception ex) {
            System.err.println("Erro ao Criptografar: " + ex);
            return "";
        }
    }
    
    // Gravar os dados no XML
    public static void gravar(ArrayList player){
        xml = xstream.toXML(player);
        try{
            try (FileWriter w = new FileWriter("memorycard.xml")) {
                w.write(xml);
            }
        }catch(IOException e){
            System.err.println("Erro ao gravar o XML: " + e);
        }
    }
    // Ler os dados do XML    
    public static String ler() {
        try {
            StringBuilder sb;
            try (Scanner in = new Scanner(new File("memorycard.xml"))) {
                sb = new StringBuilder();
                while (in.hasNext()) {
                    sb.append(in.nextLine());
                }
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            System.err.println("Erro ao ler o XML: " + e);
        }
        return "";
    }
    
}
